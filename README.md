A calculator which can perform the following operations:

-Addition
-Subtraction
-Multiplication
-Division
-Powers
-Roots

in the decimal, hexadecimal and octal systems.

This program was made to be run on an Arduino board for a Digital Circuits project.