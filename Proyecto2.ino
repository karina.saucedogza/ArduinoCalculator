/*Calculadora que realiza las operaciones de suma, resta, multiplicación, división, raíz enésima y potencia enésima en los sistemas decimal y hexadecimal*/
#include <LiquidCrystal.h> //Libreria lcd
#include <Keypad.h> //Libreria teclado
LiquidCrystal lcd(A0,A1,A2,A3,A4,A5); //Pines del lcd
const byte FILAS = 4; // Número de filas del teclado
const byte COLUMNAS = 4; // Número de columnas del teclado
//Asignación de teclas
char teclas [FILAS] [COLUMNAS] = {
  {'1', '2', '3', '+'},
  {'4', '5', '6', '-'},
  {'7', '8', '9', '*'},
  {'C', '0', '=', 'Y'},
};
byte pinesFilas[FILAS] = {5, 4, 3, 2}; //Pines de las filas de teclado
byte pinesColumnas[COLUMNAS] = { 6,7, 8, 9}; //Pines de las columnas del teclado
//Creación de un objeto de tipo Keypad
Keypad teclado = Keypad( makeKeymap(teclas), pinesFilas, pinesColumnas, FILAS, COLUMNAS);
//Declaración de variables globales
boolean primerValor= false; //Determina si se ha terminado de escribir el primer valor
boolean final = false;//Determina si el usuario ha terminado de introducir la operación a realizar por completo
String num1, num2;//Cadenas de caracteres para capturar los valores introducidos por el usuario
float resp;//Almacena la respuesta de la operación
float resphexa;
String respuesta;
String respuestahexa;
char opcion;//Almacena el operando de la operación a realizar
boolean hexadecimal=false;//Determina si el usuario introdujo un número en hexadecimal
int bandera=0;
int bandera2=0;
void setup() {
  //Código de inicialización 
  lcd.begin(16,2);
  lcd.setCursor(2,0);
  lcd.print("()()");
  lcd.setCursor(2,1);
  lcd.print("(-.-)");
  delay(2500);
  lcd.clear(); 
}
void loop() {
  //Código del programa
  char tecla=teclado.getKey();
  if (tecla!= NO_KEY && (tecla=='1'||tecla=='2'||tecla=='3'||tecla=='4'||tecla=='5'||tecla=='6'||tecla=='7'||tecla=='8'||tecla=='9'||tecla=='0')){//Condición para leer la tecla presionada por el usuario
    if (primerValor!= true){//Condición para seguir leyendo mientras no se haya terminado de introducir el primer valor
      if(bandera==1 and tecla=='1'){
        tecla='a';
      }
      if(bandera==1 and tecla=='2'){
        tecla='b';
      }
      if(bandera==1 and tecla=='3'){
        tecla='c';
      }
      if(bandera==1 and tecla=='4'){
        tecla='d';
      }
      if(bandera==1 and tecla=='5'){
        tecla='e';
      }
      if(bandera==1 and tecla=='6'){
        tecla='f';
      }
      num1 = num1 + tecla;//Asignación de las teclas presionadas por el usuario a la variable num1
      int longitudNum = num1.length();//Determina la longitud de la variable num1 (cantidad de caracteres)
      lcd.setCursor(15 - longitudNum, 0); //Deja un espacio en blanco después del número moviendo el cursor en el lcd
      lcd.print(num1);//Muestra lo que el usuario introduce en el lcd
    }
    else{
      if(bandera==1 and tecla=='1'){
        tecla='a';
      }
      if(bandera==1 and tecla=='2'){
        tecla='b';
      }
      if(bandera==1 and tecla=='3'){
        tecla='c';
      }
      if(bandera==1 and tecla=='4'){
        tecla='d';
      }
      if(bandera==1 and tecla=='5'){
        tecla='e';
      }
      if(bandera==1 and tecla=='6'){
        tecla='f';
      }
      num2 = num2 + tecla;//Asignación de las teclas presionadas por el usuario a la variable num2
      int longitudNum = num2.length();//Determina la longitud de la variable num2 (cantidad de caracteres)
      lcd.setCursor(15 - longitudNum, 1);//Deja un espacio en blanco después del número moviendo el cursor en el lcd
      lcd.print(num2);//Muestra lo que el usuario introduce en el lcd
      final = true;//Se le asigna el valor de verdadero a la variable final puesto que el usuario ha introducido todos los datos necesarios
    }
  }
    else if (primerValor == false && tecla != NO_KEY && (tecla == '/' || tecla == '*' || tecla == '-' || tecla == '+')){//Si aun no se introduce el primer valor pero se presionan las teclas de algún operador
      if (primerValor == false){//Si el primer valor no se ha introducido 
      primerValor = true;//La condición cambia a verdadero cuando se presiona un operador. El primer valor ya se ha introducido.
      opcion = tecla;//La variable opción toma el valor de la tecla presionada por el usuario
      if(bandera2==1 && tecla=='+'){
        opcion='/';
      }
      if(bandera2==1 && tecla=='-'){
        opcion='^';
      }
      if(bandera2==1 && tecla=='*'){
        opcion='r';
      }
      lcd.setCursor(15,0); //Mover el cursor para mostrar el operador
      lcd.print(opcion);//Mostrar la opción en el lcd
    }
  }
  else if (final == true && tecla!= NO_KEY && tecla == '='){//Condición que se cumple si la variable final es verdadera y se presiona la tecla de igual que
    for(int i=0;i<num1.length();i++){
      if(num1[i]=='a'||num1[i]=='b'||num1[i]=='c'||num1[i]=='d'||num1[i]=='e'||num1[i]=='f'){
        hexadecimal=true;
      }
    }
    for(int i=0;i<num2.length();i++){
      if(num2[i]=='a'||num2[i]=='b'||num2[i]=='c'||num2[i]=='d'||num2[i]=='e'||num2[i]=='f'){
        hexadecimal=true;
      }
    }
    if (opcion == '+'){//Si la opción es +, convertir las cadenas a valores flotantes y sumar ambos números
        float numero1=(float)convertir(num1);
        float numero2=(float)convertir(num2);
        resphexa=numero1+numero2;
        respuestahexa=String((int)resphexa, HEX);
        resp=(float)num1.toInt() +(float) num2.toInt();
        respuesta=resp;
    }
    else if (opcion == '-'){//Si la opción es -, convertir las cadenas a valores flotantes y restar ambos números
        float numero1=(float)convertir(num1);
        float numero2=(float)convertir(num2);
        resphexa=numero1-numero2;
        respuestahexa=String((int)resphexa,HEX);
        resp= (float)num1.toInt() - (float)num2.toInt();
        respuesta=resp;
    }
    else if (opcion == '*'){//Si la opción es *, convertir las cadenas a valores flotantes y multiplicar ambos números
        float numero1=(float)convertir(num1);
        float numero2=(float)convertir(num2);
        resphexa=numero1*numero2;
        respuestahexa=String((int)resphexa,HEX);
        resp= (float)num1.toInt() * (float)num2.toInt();
        respuesta=resp;
    }
    else if (opcion == '/'){
        float numero1=(float)convertir(num1);
        float numero2=(float)convertir(num2);
        resphexa=numero1/numero2;        
        respuestahexa=String((int)resphexa,HEX);
        resp= (float)num1.toInt() /(float) num2.toInt();
        respuesta=resp;
    }
    else if(opcion=='^'){
        int numero1=convertir(num1);
        int numero2=convertir(num2);
        resphexa=(pow(numero1,numero2));
        respuestahexa=String((int)resphexa,HEX);    
        resp=pow(num1.toInt(),num2.toInt());
        respuesta=resp;
    }
    else if(opcion=='r'){
        int numero1=convertir(num1);
        int numero2=convertir(num2);
        resphexa=pow(numero1,1.0/numero2);
        respuestahexa=String((int)resphexa,HEX);  
        resp=(float)pow(num1.toInt(), 1.0/num2.toInt());
        respuesta=resp;
    }
      lcd.clear();//Limpia la pantalla
      lcd.setCursor(10,0);//Mueve el cursor
      lcd.autoscroll();
      //lcd.print(respuestahexa);//Muestra la respuesta en el lcd
      if(hexadecimal==true){
        lcd.print(respuestahexa);
      }
      else{
      lcd.print("Dec: ");
      lcd.print(respuesta);
      lcd.setCursor(18,1);
      lcd.print("Hex: ");
      lcd.print(respuestahexa);
      }
      lcd.noAutoscroll();
  }
  else if (tecla != NO_KEY && tecla == 'C'){//Si se presiona la tecla C
    lcd.clear();//Limpiar la pantalla
    primerValor = false;//Se restablecen todos los valores para que el usuario pueda realizar nuevas operaciones sin problemas
    final = false;
    num1 = "";
    num2 = "";
    resp=0;
    resphexa=0;
    bandera=0;
    bandera2=0;
    hexadecimal=false;
    respuesta="";
    respuestahexa="";
    opcion =' ';
  }
  else if (tecla!= NO_KEY && tecla == 'Y'){
      //lcd.clear();//Limpiar la pantalla
      //primerValor = false;//Se restablecen todos los valores para que el usuario pueda realizar nuevas operaciones sin problemas
      //final = false;
      //num1 = "";
      //num2 = "";
      //resp=0;
      //respuesta="";
      //opcion=' ';
      if(bandera2==0){
        bandera2=1;
      }
      else{
        bandera2=0;
      }
      if(bandera==0){
        bandera=1;
      }
      else{
        bandera=0;
      }
  }
}
unsigned int convertir(String numeroHex) {
  unsigned int numero= 0;
  int siguiente;
  for (int i = 0; i < numeroHex.length(); i++) {
    siguiente = int(numeroHex.charAt(i));
    if (siguiente >= 48 && siguiente <= 57) siguiente = map(siguiente, 48, 57, 0, 9);
    if (siguiente >= 65 && siguiente <= 70) siguiente = map(siguiente, 65, 70, 10, 15);
    if (siguiente >= 97 && siguiente <= 102) siguiente = map(siguiente, 97, 102, 10, 15);
    siguiente = constrain(siguiente, 0, 15);
    numero= (numero* 16) + siguiente;
  }
  return numero;
}

